#!/bin/bash

sudo apt update && sudo apt install -y python-setuptools
pyvenv-3.5 .

echo "Now type 'source bin/activate' to enter virtualenv."

exit 0