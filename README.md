# Um servidor http simples

Esse servidor http tem como objetivo demonstrar o funcionamento do protocolo HTTP, sua interação com os protocolos das camadas inferiores.

## Preparando o ambiente virtual do python (venv)

Execute o script de configuração do ambiente

`./setupEnvironment.sh`

agora

`. bin/activate` para ativar o ambiente virtual

`deactivate` para desativar o ambiente virtual (ou simplesmente saia do terminal)